package demo;

import static org.junit.Assert.*;

import java.sql.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import demo.repositories.PlantRepository;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class, SimpleDbConfig.class})
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
public class ApplicationTests {
    @Autowired
    PlantRepository plantRepo;

    @Test
    @DatabaseSetup("dataset.xml")
    public void countAllPlants() {
        assertEquals(14, plantRepo.count());
       
        
    }

    @Test
    @DatabaseSetup("dataset.xml")
    public void countExcavators() {
        assertEquals(6, plantRepo.findByNameLike("Excavator").size());
    }

    @Test
    @DatabaseSetup("dataset.xml")
    public void countLikeNameAndPriceRange() {
        assertEquals(4, plantRepo.finderMethod("Excavator", 200.00f, 400.00f).size());
    }
    @Test
    @DatabaseSetup("dataset.xml")
    public void getThem() {
        assertEquals(1, plantRepo.getThem().size());
    }
    
    
//    @Test
//    @DatabaseSetup("dataset.xml")
//    public void countAvailableBetweenDates(){
//    	 assertEquals(1, plantRepo.findAvailablePlantsInDate(new Date(1/1/2013), new Date(1/1/2013)).size());
//    }
}
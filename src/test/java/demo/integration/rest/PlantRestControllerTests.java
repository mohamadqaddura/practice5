package demo.integration.rest;

import java.util.Date;
import java.text.SimpleDateFormat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import demo.Application;
import demo.SimpleDbConfig;
import demo.repositories.PlantRepository;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { Application.class,
		SimpleDbConfig.class })
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class PlantRestControllerTests {
	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private PlantRepository plantRepo;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	
	
	@Test
	@DatabaseSetup("dataset.xml")
	public void testGetAllPlants() throws Exception {
		mockMvc.perform(get("/rest/plants")).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(14)));
	}
	
	
//	@DatabaseSetup("dataset.xml")
//	public void testGetANonExistingPlant() throws Exception {
//		mockMvc.perform(get("/rest/plants/{id}", 10015L))
//        	.andExpect(status().isNotFound());
//	}
	
	@Test
	@DatabaseSetup("dataset.xml")
	public void testGetPlantByNameDateDate() throws Exception{
		String plant="name";
		
		   SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
//		   String startDate = dmyFormat.format(new Date (1992-01-01));
//		   String endDate = dmyFormat.format(new Date (1992-01-01));
//		    // myDate is the java.util.Date in yyyy-mm-dd format
//		    // Converting it into String using formatter
//		   // String strDate = sm.format(myDate);
//		    //Converting the String back to java.util.Date
//		   // Date dt = sm.parse(strDate);
//		
		Date startDate=new Date (200-01-01);
		Date endDate=new Date (200-01-02);

		mockMvc.perform(get("/rest/plants/nameDateDate")
				
				.param("name", plant)
	            .param("startDate", dmyFormat.format(startDate))
	            .param("endDate", dmyFormat.format(endDate)))
	            .andExpect(status().isOk())
		.andExpect(jsonPath("$", hasSize(14)));
	}
	
	
}

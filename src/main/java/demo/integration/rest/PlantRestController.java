package demo.integration.rest;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import demo.integration.dto.PlantResource;
import demo.integration.dto.PlantResourceAssembler;
import demo.models.Plant;
import demo.repositories.PlantRepository;

@RestController
@RequestMapping("/rest")
public class PlantRestController {
	@Autowired
	PlantRepository plantRepo;

	@RequestMapping("/plants")
	public List<PlantResource> getAllPlants() throws PlantNotFoundException {
		List<Plant> plant = plantRepo.findAll();
		 if (plant == null) throw new PlantNotFoundException();
		 return PlantResourceAssembler.toResource(plant);
		// plant resources
	}

	
	
	@RequestMapping(value="{id}")
	@ResponseStatus(HttpStatus.OK)
	public PlantResource getPlant(@PathVariable("id") Long id) throws Exception {
	    Plant plant = plantRepo.findOne(id);
	    if (plant == null) throw new PlantNotFoundException(id);
	        return PlantResourceAssembler.toResource(plant);
	}

	//OURS
	@RequestMapping(value="/plants/nameDateDate",method=RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<PlantResource> getPlantByNameAndDate1Date2(
			@RequestParam("name") String name,
			@RequestParam("startDate") Date startDate,
			@RequestParam("endDate") Date endDate) throws PlantNotFoundException {
		List<Plant> plant = plantRepo.findByNameDateDateLike(name,startDate,
				endDate);
		 if (plant == null) throw new PlantNotFoundException("custom error");
		 return PlantResourceAssembler.toResource(plant);
	}

	//OURS
	@ExceptionHandler(PlantNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public void handelPlantNotFoundException(PlantNotFoundException ex) {

	}
}

package demo.integration.rest;

import java.sql.Date;

public class PurchaseOrderNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public PurchaseOrderNotFoundException(Long id) {
		super(String.format("Purchase not found! (Plant id: %d)", id));
	}
	
	public PurchaseOrderNotFoundException() {
		super(String.format("Nothing Found"));
	}
	
	public PurchaseOrderNotFoundException(String input) {
		super(String.format(input));
	}
	
	public PurchaseOrderNotFoundException(String name,Date start,Date end) {
		super(String.format("Purchase not found in that date"));
	}
}

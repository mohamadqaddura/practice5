package demo.integration.rest;

import java.net.URI;
import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import demo.models.PurchaseOrder;
import demo.repositories.PlantRepository;
import demo.repositories.PurchaseOrderRepository;
import demo.integration.dto.PlantResourceAssembler;
import demo.integration.dto.PurchaseOrderResource;
import demo.integration.dto.PurchaseOrderResourceAssembler;
import demo.integration.dto.PurchaseOrderResourceList;

@RestController
@RequestMapping("/rest")
public class PurchaseOrderRESTController {
	@Autowired
	PurchaseOrderRepository purchaseOrderRepo;
	
	PurchaseOrderResourceAssembler assembler =new PurchaseOrderResourceAssembler();

	// @RequestMapping(method = RequestMethod.GET, value = "/pos")
	@RequestMapping("/pos")
	public List<PurchaseOrderResource> getAllPOs() {
		List<PurchaseOrder> pos = purchaseOrderRepo.findAll();
		return assembler.toResource(pos);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/pos")
	public ResponseEntity<PurchaseOrderResource> createPO(@RequestBody PurchaseOrderResource res) {
		PurchaseOrder po = new PurchaseOrder();
		po.setStartDate(res.getStartDate());
		po.setEndDate(res.getEndDate());
		po.setCost(res.getCost());

		// po.persist();
		purchaseOrderRepo.save(po);

			HttpHeaders headers = new HttpHeaders();
		URI location = ServletUriComponentsBuilder.fromCurrentRequestUri()
				.pathSegment(po.getId().toString()).build().toUri();
		headers.setLocation(location);

		ResponseEntity<PurchaseOrderResource> response = new ResponseEntity<>(assembler.toResource(po), headers,
				HttpStatus.CREATED);
		return response;
	}

	
	@RequestMapping(method = RequestMethod.PUT, value = "/pos/{id}")
	public ResponseEntity<PurchaseOrderResource> updatePO(
			@RequestParam("startDate") Date startDate,
			@RequestParam("endDate") Date endDate,
			@RequestParam("id") Long id
			) 
			{
		PurchaseOrder po = purchaseOrderRepo.findOne(id);
		po.setStartDate(startDate);
		po.setEndDate(endDate);
		po.setCost(po.getCost());

		// po.persist();
		purchaseOrderRepo.saveAndFlush(po);
		
		//purchaseOrderRepo.

			HttpHeaders headers = new HttpHeaders();
		URI location = ServletUriComponentsBuilder.fromCurrentRequestUri()
				.pathSegment(po.getId().toString()).build().toUri();
		headers.setLocation(location);

		ResponseEntity<PurchaseOrderResource> response = new ResponseEntity<>(assembler.toResource(po), headers,
				HttpStatus.CREATED);
		return response;
	}

	
	
	@RequestMapping(method = RequestMethod.GET, value = "/pos/{id}")
	public PurchaseOrderResource getPO(@PathVariable Long id) throws Exception {
		PurchaseOrder po = purchaseOrderRepo.findOne(id);
		// purchaseORderNOtFound
		if (po == null)
			throw new PurchaseOrderNotFoundException(id);
		return assembler.toResource(po);

	}
}

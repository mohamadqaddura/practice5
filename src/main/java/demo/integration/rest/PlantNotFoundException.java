package demo.integration.rest;

import java.sql.Date;

public class PlantNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public PlantNotFoundException(Long id) {
		super(String.format("Plant not found! (Plant id: %d)", id));
	}
	
	public PlantNotFoundException() {
		super(String.format("Nothing Found"));
	}
	
	public PlantNotFoundException(String input) {
		super(String.format(input));
	}
	
	public PlantNotFoundException(String name,Date start,Date end) {
		super(String.format("Plant not found in that date"));
	}
}

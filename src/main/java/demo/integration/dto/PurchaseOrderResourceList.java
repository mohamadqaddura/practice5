package demo.integration.dto;

import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;


@Data
@XmlRootElement(name="purchaseOrders")
public class PurchaseOrderResourceList {
	
	private List<PurchaseOrderResource> purchaseOrder;
	
	public PurchaseOrderResourceList() {
		this(Collections.<PurchaseOrderResource>emptyList());
	}
	public PurchaseOrderResourceList(List<PurchaseOrderResource> pos) {
		this.purchaseOrder = pos;
	}
}

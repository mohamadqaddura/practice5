package demo.integration.dto;

import java.sql.Date;

import javax.xml.bind.annotation.XmlRootElement;

import demo.models.Customer;
import lombok.Data;


@Data
@XmlRootElement(name="PurchaseOrder")
public class PurchaseOrderResource {
	Long id;

	Customer customer;

	PlantResource plant;

	Date startDate;
	Date endDate;
	
	String contact;
	Float cost;

	public Float getTotal(){
		return cost;
	}
	public void setTotal(Float amount){
		cost=amount;		
	}
	
}

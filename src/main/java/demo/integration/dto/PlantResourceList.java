package demo.integration.dto;

import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;


@Data
@XmlRootElement(name="plants")
public class PlantResourceList {
	
	private List<PlantResource> plant;
	
	public PlantResourceList() {
		this(Collections.<PlantResource>emptyList());
	}
	public PlantResourceList(List<PlantResource> plants) {
		this.plant = plants;
	}
}

package demo.integration.dto;

import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import demo.models.PurchaseOrder;

public class PurchaseOrderResourceAssembler {

	public PurchaseOrderResource toResource(PurchaseOrder po) {
		PurchaseOrderResource res = new PurchaseOrderResource();

		res.setStartDate((Date) po.getStartDate());
		res.setEndDate((Date) po.getEndDate());
		res.setCost((Float) po.getCost());
		res.setContact(res.getContact());
		PlantResource plantRes = null;

		if (po.getPlant() != null) {
			PlantResourceAssembler assembler = new PlantResourceAssembler();
			plantRes = assembler.toResource(po.getPlant());
		
		}

		res.setPlant(plantRes);
		return res;
	}

	public List <PurchaseOrderResource> toResource(List<PurchaseOrder> pos) {
		List<PurchaseOrderResource> resList = new ArrayList<>();
		for (PurchaseOrder po : pos)
			resList.add(toResource(po));
		return resList;
	}
}

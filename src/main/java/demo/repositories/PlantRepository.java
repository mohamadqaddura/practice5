package demo.repositories;

//import java.sql.Date;
import java.util.Date;
import java.util.List;

import org.crsh.cli.impl.line.Quoting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import demo.models.Plant;
//import demo.models.PurchaseOrder;


@Repository
public interface PlantRepository extends JpaRepository<Plant, Long> {

	List<Plant> findByNameLike(String name);

	//find using name, maximum price, minimum price
	@Query("select p from Plant p "
			+ "where LOWER(p.name) like LOWER(:name) and p.price between :minimum and :maximum")
	List<Plant> finderMethod(
			@Param("name") String name,
			@Param("minimum") Float minimum,
			@Param("maximum") Float maximum);
	
	@Query("select p from Plant p, PurchaseOrder o WHERE o.plant=p.id")
	List<Plant> getThem();
	
	
//   find available between two dates
   @Query("SELECT p FROM Plant p, PurchaseOrder po WHERE po.plant=p.id "
	          + "and po.startDate > :endDate or po.endDate < :startDate")
	    List<Plant> findAvailablePlantsInDate(
			   @Param("endDate") Date endDate,
			  @Param("startDate") Date startDate);
   
   //find a plant by name and date date
	@Query("SELECT p FROM Plant p, PurchaseOrder po WHERE po.plant=p.id "
	          + "and po.startDate > :endDate or po.endDate < :startDate and LOWER(p.name) LIKE LOWER(:name)")
	List<Plant> findByNameDateDateLike(
			@Param("name") String Name,
			@Param("startDate") Date Date1,
			@Param("endDate") Date Date2);
	
	//Select p FROM Plant p INNER JOIN PurchaseOrder po ON po.plant=p.id0
	
//	@Query("SELECT p FROM Plant p, PurchaseOrder po WHERE po.plant=p.id "
//	          + "and po.startDate > :endDate or po.endDate < :startDate and LOWER(p.name) LIKE LOWER(:name)")
//	List<Plant> findByNameDateDateLikeS(
//			@Param("name") String Name,
//			@Param("startDate") String Date1,
//			@Param("endDate") String Date2);
}



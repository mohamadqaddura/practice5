package demo.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import demo.models.Plant;
import demo.models.PurchaseOrder;

@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Long>{

	List<PurchaseOrder> findByContactLike(String contact);
	

	@Query("SELECT po FROM  PurchaseOrder po WHERE po.plant=:plantId AND po.startDate=:startDate and po.endDate = :endDate")
	List<PurchaseOrder> findByNameDateDateLike(
			@Param("plantId") Long id,
			@Param("startDate") Date Date1,
			@Param("endDate") Date Date2);
	
}

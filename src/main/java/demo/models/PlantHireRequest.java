package demo.models;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@Data
@XmlRootElement
public class PlantHireRequest {
	@Id
	@GeneratedValue
	Long id;
	
	@OneToOne
	Plant plant;
	@OneToOne
	SiteEngineer siteEngineer;
	@OneToOne
	WorkEngineer workEngineer;
	
	@Temporal(TemporalType.DATE)
	Date startDate;
	
	@Temporal(TemporalType.DATE)
	Date endDate;
	
	Float price;
}

package demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import demo.repositories.PlantHireRequestRepository;

@Controller
@RequestMapping("/phrs")
public class PHRController {
	
	@Autowired
	 PlantHireRequestRepository repo;
	
	
	@RequestMapping(value="/showx")
	public String hi(){
		return "phrs/show";
	}
	@RequestMapping(value="/show")	
	public String list(Model model){
		model.addAttribute("phr", repo.findAll());
		//model.addAttribute(arg0)
		return "phrs/show";
	}
}
